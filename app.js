const express = require('express');
const app = express();
const port = 3000;

// Function to generate a random true or false value
const getRandomBoolean = () => Math.random() < 0.5;

// Define API endpoints with random values
app.get('/api/100', (req, res) => {
    res.json({ gateId: 100, value: getRandomBoolean() });
    console.log("100");
});

app.get('/api/200', (req, res) => {
    res.json({ gateId: 200, value: getRandomBoolean() });
    console.log("200");
});

app.get('/api/300', (req, res) => {
    res.json({ gateId: 300, value: getRandomBoolean() });
    console.log("300");
});

app.get('/api/400', (req, res) => {
    res.json({ gateId: 400, value: getRandomBoolean() });
    console.log("400");
});

app.get('/api/500', (req, res) => {
    res.json({ gateId: 500, value: getRandomBoolean() });
    console.log("500");
});

// Start the server
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
